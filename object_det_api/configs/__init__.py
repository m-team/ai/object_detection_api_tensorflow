"""Configuration loader for rnacontactmap."""
import configparser
import ast
import os
import pathlib
from importlib.metadata import metadata as _metadata

homedir = os.path.dirname(os.path.normpath(os.path.dirname(__file__)))
base_dir=os.path.dirname(os.path.abspath(homedir))


# Get configuration from user env and merge with pkg settings
SETTINGS_FILE = pathlib.Path(__file__).parent / "settings.ini"
SETTINGS_FILE = os.getenv("object_detection", default=SETTINGS_FILE)
settings = configparser.ConfigParser()
settings.read(SETTINGS_FILE) 

def resolve_path(base_dir):
    if os.path.isabs(base_dir):
        return base_dir
    else:
        return os.path.abspath(os.path.join(homedir, base_dir))
    
base_dir=resolve_path(base_dir)
try:  # Configure input files for testing and possible training
    DATA_PATH = os.getenv("DATA_PATH", default=settings['data']['path'])
    os.environ["DATA_PATH"] = os.path.join(base_dir, DATA_PATH)
except KeyError as err:
    raise RuntimeError("Undefined configuration for [data]path") from err

try:  # Local path for caching   sub/models
    MODEL_DIR = os.getenv("MODEL_DIR", settings['model_dir']['path'])
    os.environ["MODEL_DIR"] = os.path.join(base_dir, MODEL_DIR)
except KeyError as err:
    raise RuntimeError("Undefined configuration for model path") from err

try:  # Local path for caching    
    DEFAULT_MODEL = os.getenv("DEFAULT_MODEL", settings['default_model_path']['path'])
    os.environ["DEFAULT_MODEL"] = os.path.join( MODEL_DIR , DEFAULT_MODEL) 
except KeyError as err:
    raise RuntimeError("Undefined configuration for model default path") from err

try:  # Local path for caching    
    DEFALUT_LABLE_MAP = os.getenv("DEFALUT_LABLE_MAP", settings['default_model_label_path']['path'])
    os.environ["DEFALUT_LABLE_MAP"] = os.path.join( MODEL_DIR , DEFAULT_MODEL) 
except KeyError as err:
    raise RuntimeError("Undefined configuration for default label map path") from err

try:   
    MODEL_NAME = os.getenv("MODEL_NAME", settings['model_config_name']['names'])
    if isinstance(MODEL_NAME, str):
        # Parse the string as a list of strings
        MODEL_NAME = ast.literal_eval(MODEL_NAME)
except KeyError as err:
    raise RuntimeError("Undefined configuration for MODEL_NAME") from err


