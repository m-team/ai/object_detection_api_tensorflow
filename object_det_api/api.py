# -*- coding: utf-8 -*-
"""
Functions to integrate your model with the DEEPaaS API.
It's usually good practice to keep this file minimal, only performing the interfacing
tasks. In this way you don't mix your true code with DEEPaaS code and everything is
more modular. That is, if you need to write the predict() function in api.py, you
would import your true predict function and call it from here (with some processing /
postprocessing in between if needed).
For example:

    import mycustomfile

    def predict(**kwargs):
        args = preprocess(kwargs)
        resp = mycustomfile.predict(args)
        resp = postprocess(resp)
        return resp

To start populating this file, take a look at the docs [1] and at a canonical exemplar
module [2].

[1]: https://docs.deep-hybrid-datacloud.eu/
[2]: https://github.com/deephdc/demo_app
"""

 
import os
from pathlib import Path
import shutil
import subprocess
import sys
import tempfile
import logging 
from datetime import datetime 

 
from object_det_api import fields ,utils ,configs 
from object_det_api.scripts import inference
from object_detection import model_main_tf2

 
logger = logging.getLogger('__name__')


def get_metadata():
    """
    Returns a dictionary containing metadata information about the module.

    Returns:
        A dictionary containing metadata information required by DEEPaaS.
    """
    metadata = {
        'authors': configs.MODEL_METADATA.get("author"),
        'description': configs.MODEL_METADATA.get("summary"),
        'license': configs.MODEL_METADATA.get("license"),
        'version': configs.MODEL_METADATA.get("version"),
        }
    logger.debug("Package model metadata: %d", metadata)
    return  metadata


def get_train_args():
    """
    Return the arguments that are needed to perform a  training.

    Returns:
        Dictionary of webargs fields.
    """
    train_args=fields.TrainArgsSchema().fields
    logger.debug("Web arguments: %d", train_args) 
    return  train_args



def preprocess(**args):
     
     return {}

def train(**args):
    """
    Performs training on the dataset.
    Args:
        **args: keyword arguments from get_train_args.
    Returns:
        path to the trained model
    """
    #First create tf record and save them in the related path
    args['train_input_path']=utils.creat_tf_record(args['input_path'], args['label_map_path'],'train')
    args['eval_input_path']=utils.creat_tf_record(args['input_path'], args['label_map_path'],'val')
    data_configs_path=os.path.join(configs.DATA_PATH,'configs', str(args['model_config_name'])+'.config')
    timestamp=datetime.now().strftime('%Y-%m-%d_%H%M%S')
    #path to save the model
    args['model_dir']=os.path.join(configs.MODEL_DIR,timestamp)
    with tempfile.TemporaryDirectory() as temp_dir:
            filename = os.path.basename(data_configs_path)
            dest_path= os.path.join(temp_dir, filename)
            shutil.copy(data_configs_path, dest_path)
            utils.update_config_user_args(dest_path, args)
            args['pipeline_config_path']= os.path.join(os.path.dirname(dest_path),'pipeline.config')
  
            flag_names=utils.get_flag_names(args, model_main_tf2.FLAGS)
            command = [sys.executable, "-m", "object_detection.model_main_tf2"]
            for flag_name in flag_names:
                flag_value = str(args[flag_name.lstrip('--')])
                command.extend([flag_name, str(flag_value)])
            subprocess.run(command)

            return {f"model was saved in the {args['model_dir']}"}

def get_predict_args():
    """
    Return the arguments that are needed to perform inference.

    Returns:
        Dictionary of webargs fields.
    """
    predict_args=fields.PredictArgsSchema().fields
    logger.debug("Web arguments: %d", predict_args) 
    return predict_args


def predict(**args):
    """
    Performs inference  on an input image.
    Args:
        **args:   keyword arguments from get_predict_args.
    Returns:
        either a json file or png image with bounding box 
    """
    with tempfile.TemporaryDirectory() as tmpdir: 
        for f in [args['input']]:
           shutil.copy(f.filename, tmpdir + F'/{f.original_filename}')
        args['input'] =[os.path.join(tmpdir,t) for t in os.listdir(tmpdir)]
        json_string , io_buf= inference.main(args) 
        if args['accept']== 'image/png':
             return io_buf
        else:
            return json_string
    




if __name__=='__main__':

     fileds=get_train_args()
     args={}
     for key,  value in fileds.items():
            if value.missing:
               args[key]=value.missing
    
         
     args['train_input_path']='/home/se1131/object_det_api/data/val.record'
     args['label_map_path']='/home/se1131/object_det_api/data/label_map.pbtext'
     args['eval_input_path']='/home/se1131/object_det_api/data/test.record'
     args['model_config_name']='ssd_efficientdet_d5_1280x1280_coco17_tpu-32'
      
     args['fine_tune_checkpoint']=""#FIXME: check if this will update in the config file
     train(**args)
   
  