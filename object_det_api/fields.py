 
import json
from webargs import fields, validate
from marshmallow import Schema, ValidationError, fields
from object_det_api import configs

#####################################################
#  Options to  train your model
#####################################################
class TrainArgsSchema(Schema):

    class Meta:
        ordered = True

    model_config_name= fields.Str(
        enum= configs.MODEL_NAME,
        required=True,
        description= 'Name of the model for training' 
                   ) 
    
    input_path= fields.Str(
        required=True,
        description= 'train_input_path.')
    
    label_map_path= fields.Str(
        required=True,
        description= 'label_map_path.')
    

    num_classes= fields.Int(
        required=False,
        missing=4,
        description= 'number of classes.')
    
    learning_rate= fields.Float(
        required=False,
        missing=0.001,
        description= ' learning rate.')
     
    num_train_steps= fields.Int(
        required=False,
        missing=4,
        description= 'train_steps.') 
    
 #   image_resizer= fields.Int(
 #       required=False,
 #       missing=[640, 640],
 #       description= 'image [height, width] to feed to the network.') 
    
    num_classes= fields.Int(
        required=False,
        missing=4,
        description= 'number of classes.')
    
    batch_size= fields.Int(
        required=False,
        missing=4,
        description= 'batch size to load the data.')
    
    fine_tune_checkpoint= fields.Str(
        required=False,
        missing="",
        description= 'path to the ckp for fine tuning.')
    
    eval_timeout= fields.Int(
        required=False,
        missing= 3600,
        description= 'Number of seconds to wait for an'
                    ' evaluation checkpoint before exiting.')
    
    num_workers= fields.Int(
        required=False,
        missing= 1,
        description='When num_workers > 1, training uses '
                    'MultiWorkerMirroredStrategy. When num_workers = 1 it uses '
                    'MirroredStrategy.')
    
    
    checkpoint_dir= fields.Str(
        required=False,
        description= 'checkpoint_dir is provided, this binary operates in eval-only'
          ' mode, writing resulting metrics to `model_dir`.')
    
    eval_num_epochs= fields.Int(
        required=False,
        missing=4,
        description= 'eval_num_epochs.')
    
    classification_localization_weight_ratio= fields.Float(
        required=False,
        missing=1.0,
        description= 'classification_localization_weight_ratio..')

    focal_loss_gamma= fields.Float(
        required=False,
        missing=1.0,
        description= 'focal_loss_gamma.')
    
    focal_loss_alpha= fields.Float(
        required=False,
        missing=1.0,
        description= 'focal_loss_alpha.')

    eval_with_moving_averages= fields.Bool(
        required=False,
        missing=True,
        enum=[True,False],
        description= 'eval_with_moving_averages.')

    sample_1_of_n_eval_examples= fields.Int(
        required=False,
        missing=4,
        description= 'sample_1_of_n_eval_examples'
                        'Will sample one of every n eval input examples, where n is provided..')
    
    sample_1_of_n_eval_on_train_examples= fields.Int(
        required=False,
        missing=4,
        description= 'sample_1_of_n_eval_examples'
                        'Will sample one of every n eval input examples, where n is provided..')
    eval_on_train_data= fields.Bool(
        required=False,
        missing=False,
        enum=[True,False],
        description= 'Enable evaluating on train '
                  'data (only supported in distributed training).')
    use_bfloat16= fields.Bool(
        required=False,
        missing=False,
        enum=[True,False],
        description= 'use_bfloat16')
    
    use_tpu= fields.Bool(
        required=False,
        missing=False,
        enum=[True,False],
        description= 'Whether the job is executing on a TPU.') 
    
    tpu_name= fields.Str(
        required=False,
        description= 'Name of the Cloud TPU for Cluster Resolvers.')
    
    checkpoint_every_n= fields.Int(
        required=False,
        missing=4,
        description= 'Whether or not to record summaries defined by the model or the training pipeline. This does not impact the summaries of the loss values which are always recorded.')
    
    record_summaries= fields.Bool(
        required=False,
        missing=False,
        enum=[True,False],
        description= 'Whether or not to record summaries defined by the model' 
                       ' or the training pipeline. This does not impact the' 
                        ' summaries of the loss values which are always'
                        'recorded.') 
    
    disable_wandb=fields.Bool(#FIXME: remove or add wandb
        required=False,
        missing=True,
        description= 'whether to use the wandb'
    )
   
    
#####################################################
#  Options to test your model
#####################################################

class PredictArgsSchema(Schema):
    
    class Meta:
        ordered = True

    input= fields.Field(
        required=True,
        type="file",
        location="form",
        description= 'Input an image.')    

    path_to_model= fields.Str(
        required=False,
        description= 'Model timestamp to be used for prediction. To see the available timestamp,'
              ' please run the get_metadata function. If no timestamp is given,'
              ' the model will be loaded from coco will be loaded.')
    
    label_map_path= fields.Str(
        required=True,
        description= 'label_map_path.')

    threshold= fields.Float(
        required=False,
        missing=0.5,
        description= 'detection threshold.')
    
    accept= fields.Str(
        missing ="application/pdf",
        location="headers",
        validate =validate.OneOf(['image/png', 'application/json']) ,
        description ="Returns png file with detection resutlts or a json with the prediction.")
 
'''   

class MaskType(fields.String):
    """  mask_type: A string name representing a value of
      input_reader_pb2.InstanceMaskType
    DELETE: XXXXXX Something in other line ?XXXXXXXXXXX.
    """

    def __init__(self, *, metadata=None, **kwds):
        metadata = metadata or {}
        metadata['description'] = self.__doc__
        metadata['location'] = 'headers'
       # kwds['validate'] = validate.OneOf([])
        super().__init__(metadata=metadata, **kwds)
class retain_original_image_additional_channels_in_eval(fields.Boolean):
    """retain_original_image_additional_channels_in_eval
    """

    def __init__(self, *, metadata=None, **kwds):
        metadata = metadata or {}
        metadata['description'] = self.__doc__
        super().__init__(metadata=metadata, **kwds)  
        
        
              

class peak_max_pool_kernel_size(fields.Int):
    """sUpdates the max pool kernel size (NMS) for keypoints in CenterNet.
    DELETE: XXXXXX Something in other line ?XXXXXXXXXXX.
    """
    def __init__(self, *, metadata=None, **kwds):
        metadata = metadata or {}
        metadata['description'] = self.__doc__
        kwds['missing']=4
        super().__init__(metadata=metadata, **kwds)  

class candidate_ranking_mode(fields.Str):
    """Updates how keypoints are snapped to candidates in CenterNet.
    DELETE: XXXXXX Something in other line ?XXXXXXXXXXX.
    """

    def __init__(self, *, metadata=None, **kwds):
        metadata = metadata or {}
        metadata['description'] = self.__doc__
        kwds['validate'] = validate.OneOf(["min_distance", "score_distance_ratio",
                  "score_scaled_distance_ratio", "gaussian_weighted"])
        super().__init__(metadata=metadata, **kwds)
 #elif field_name == "sample_from_datasets_weights":
  #      _update_sample_from_datasets_weights(configs["train_input_config"], value)
 
#  elif field_name == "candidate_search_scale":
 #   _update_candidate_search_scale(configs["model"], value)

  elif field_name == "score_distance_offset":
        _update_score_distance_offset(configs["model"], value)
  elif field_name == "box_scale":
    _update_box_scale(configs["model"], value)
  elif field_name == "keypoint_candidate_score_threshold":
    _update_keypoint_candidate_score_threshold(configs["model"], value)
  elif field_name == "rescore_instances":
    _update_rescore_instances(configs["model"], value)
  elif field_name == "unmatched_keypoint_score":
    _update_unmatched_keypoint_score(configs["model"], value)
  elif field_name == "score_distance_multiplier":
    _update_score_distance_multiplier(configs["model"], value)
  elif field_name == "std_dev_multiplier":
    _update_std_dev_multiplier(configs["model"], value)
  elif field_name == "rescoring_threshold":
    _update_rescoring_threshold(configs["model"], value)        
'''    
