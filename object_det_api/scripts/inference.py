from io import BytesIO
import json
import cv2
import numpy as np
import os
import sys
import tensorflow as tf
import time


from collections import defaultdict
from matplotlib import pyplot as plt
from PIL import Image

from object_detection.utils import ops as utils_ops
from object_detection.utils import label_map_util
from object_detection.utils import visualization_utils as viz_utils
from object_det_api import  configs

def load_image_into_numpy_array(path):
  """Load an image from file into a numpy array.

  Puts image into numpy array to feed into tensorflow graph.
  Note that by convention we put it into a numpy array with shape
  (height, width, channels), where channels=3 for RGB.

  Args:
    path: a file path (this can be local or on colossus)

  Returns:
    uint8 numpy array with shape (img_height, img_width, 3)
  """
  img_data = tf.io.gfile.GFile(path, 'rb').read()
  image = Image.open(BytesIO(img_data))
  (im_width, im_height) = image.size
  return np.array(image.getdata()).reshape(
      (im_height, im_width, 3)).astype(np.uint8)


def main(**args):
  
  if args['path_to_model'] is None:
     args['path_to_model']=configs.DEFAULT_MODEL  #FIXME:define this path in init
     args['label_map_path']=configs.DEFALUT_LABLE_MAP 
  print('loading the model ...')   
  start_time = time.time()   
  tf.keras.backend.clear_session() 
  detect_fn = tf.saved_model.load(args['path_to_model'])
  end_time = time.time()
  elapsed_time = end_time - start_time
  print('finished loading the model in: ' + str(elapsed_time) + 's')
    
  # Loads labels
  label_map = label_map_util.load_labelmap(args['label_map_path'])
  categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=args['num_classes'], use_display_name=True)
  category_index = label_map_util.create_category_index(categories)

 
  test_image_path = args['input_files']
  for image_path in test_image_path:
    print('Evaluating:', image_path)
    image_np=load_image_into_numpy_array(image_path)
    input_tensor = np.expand_dims(image_np, 0)
    start_time = time.time()
    detections = detect_fn(input_tensor)
    end_time = time.time()
   
    # Visualization of the results of a detection.
    image_np_with_detections = image_np.copy()
    viz_utils.visualize_boxes_and_labels_on_image_array(
        image_np_with_detections,
        detections['detection_boxes'][0].numpy(),
        detections['detection_classes'][0].numpy().astype(np.int32),
        detections['detection_scores'][0].numpy(),
        category_index,
        use_normalized_coordinates=True,
        max_boxes_to_draw=200,
        min_score_thresh=args['threshold'],
        agnostic_mode=False)
    io_buf = BytesIO(image_np)
    io_buf.seek(0)
    
  result={}
  result['detection_boxes']=detections['detection_boxes'][0].numpy().tolist()
  result['detection_classes']=detections['detection_classes'][0].numpy().astype(np.int32).tolist()
  result['detection_scores']=detections['detection_scores'][0].numpy().tolist()
  json_string = json.dumps(result)
  return  json_string , io_buf

if __name__=='__main__':
    args={}
    args['input_files']=['/home/se1131/object_det_api/models/default_model/dog.jpg']
    args['num_classes']= 90
    args['label_map_path']='/home/se1131/object_det_api/models/default_model/coco_label_map.pbtxt'
    args['path_to_model']='/home/se1131/object_det_api/models/default_model/ssd_mobilenet_v2_fpnlite_320x320_coco17_tpu-8/saved_model/'    
    main(**args)