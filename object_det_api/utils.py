
import os
import sys

from object_detection.utils import config_util as cfg 
import subprocess

def update_fine_tune_checkpoint(configs, fine_tun_checkpint):
 
  configs['train_config'].fine_tune_checkpoint  = fine_tun_checkpint
  
def update_image_size(model_config, img_size):
      meta_architecture = model_config.WhichOneof("model")
      if meta_architecture == "faster_rcnn":
        model_config.faster_rcnn.fixed_shape_resizer.height=img_size[0]
        model_config.faster_rcnn.fixed_shape_resizer.width=img_size[1]
      if meta_architecture == "ssd":
        model_config.ssd.image_resizer.fixed_shape_resizer.height=img_size[0]
        model_config.ssd.image_resizer.fixed_shape_resizer.width=img_size[1]
    


def update_config_user_args(config_data_path,user_args):
  """Checks key type and updates `configs` with the key value pair accordingly.

  Args:
    configs: Dictionary of configuration objects. See outputs from
      get_configs_from_pipeline_file() or get_configs_from_multiple_files().
    key: String indicates the field(s) to be updated.
    value: Value used to override existing field value.

  Returns:
    A boolean value that indicates whether the override succeeds.

  Raises:
    ValueError: when the key string doesn't match any of the formats above.
  """
  config =  cfg.get_configs_from_pipeline_file(config_data_path)
  keys=user_args.keys()

  for key in keys:
        status=cfg._maybe_update_config_with_key_value(config, key, user_args[key] )
        print(f'The status for the {key} is {status}')
        if  key=='fine_tune_checkpoint':
             update_fine_tune_checkpoint(config,user_args[key])
        if  key=='image_resizer':   
            update_image_size(config["model"] ,user_args[key])         
  pipeline_from_configs(config, config_data_path)            

def   pipeline_from_configs(config, path):
    pipeline_config=cfg.create_pipeline_proto_from_configs(config)
    directory = os.path.dirname(path)
    cfg.save_pipeline_config(pipeline_config, directory)


def creat_tf_record(path_to_dataset, path_to_label_map,process):
      output_path=os.path.dirname(path_to_dataset)
      if not os.path.exists(output_path):
        # Create the directory and any intermediate directories if they don't exist
           os.makedirs(output_path)
      subprocess.run([
     sys.executable,
    "-m", "object_det_api.scripts.create_tfrecord",
    "--xml_dir", os.path.join(path_to_dataset,'labels'),
    "--labels_path",  path_to_label_map,
    "--output_path",  os.path.join(output_path,f'{process}.record'),
    "--image_dir", os.path.join(path_to_dataset,'img'),  
])
      return os.path.join(output_path,f'{process}.record')

def export_graph_inference(out_dir, path_to_pipline,path_to_ckpt):
    
      subprocess.run([
     sys.executable,
    "-m", "object_detection.export_inference_graph",
    "--input_type", 'image_tensor',
    "--pipeline_config_path",  path_to_pipline,
    "----trained_checkpoint_prefix", path_to_ckpt, #=/content/train/model.ckpt-25000 
])
  
def get_flag_names(args, model):
        flag_names = []
        for attr_name in dir(model):
           if not attr_name.startswith('__') and attr_name in args.keys():
              flag_names.append('--' + attr_name)
        return flag_names

